using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TextEditor
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult dialognew = MessageBox.Show("Do you want to save it before?", "New", MessageBoxButtons.YesNo);

            if (dialognew == DialogResult.Yes)
            {
                SaveFileDialog sfdial = new SaveFileDialog();
                sfdial.Filter = "Files (.txt)|*.txt";
                sfdial.Title = "Save a file";

                if (sfdial.ShowDialog() == DialogResult.OK)
                {
                    System.IO.StreamWriter serialf = new System.IO.StreamWriter(sfdial.FileName);
                    serialf.Write(richTextBox1.Text);
                    serialf.Close();
                }
            }

            else if (dialognew == DialogResult.No)
            {
                richTextBox1.Clear();
            }
            
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        public void richTextBox1_TextChanged(object sender, EventArgs e)
        {
           
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Files (.txt)|*.txt";
            ofd.Title = "O a file";

            if(ofd.ShowDialog() == DialogResult.OK)
            {
                System.IO.StreamReader les = new System.IO.StreamReader(ofd.FileName);
                richTextBox1.Text = les.ReadToEnd();
                les.Close();
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfdial = new SaveFileDialog();
            sfdial.Filter = "Files (.txt)|*.txt";
            sfdial.Title = "Save a file";

            if (sfdial.ShowDialog() == DialogResult.OK)
            {
                System.IO.StreamWriter serialf = new System.IO.StreamWriter(sfdial.FileName);
                serialf.Write(richTextBox1.Text);
                serialf.Close();
            }
        }

        private void cutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.Cut();
        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.Copy();
        }

        private void pasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.Paste();
        }

        private void undoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.Undo();
        }

        private void redoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.Redo();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (Form2 form2 = new Form2())
            {
                form2.ShowDialog();
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void Form_Closing(object sender, FormClosingEventArgs e)
        {
            DialogResult dialog = MessageBox.Show("Do you want to close it?", "Exit", MessageBoxButtons.YesNo);

            if (dialog == DialogResult.Yes)
            {
                Application.Exit();
            }

            else if (dialog == DialogResult.No)
            {
                e.Cancel = true;
            }

        }

        public void findToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (Form3 form3 = new Form3(this))
            {
                form3.ShowDialog();
            }
        }

    }
}
