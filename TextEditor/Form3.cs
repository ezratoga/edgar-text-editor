using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;



namespace TextEditor
{
    public partial class Form3 : Form
    {
        Form1 f1;
        public Form3(Form1 frm1)
        {
            InitializeComponent();
            this.f1=frm1;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int res = 0;
            string temp = f1.richTextBox1.Text;
            temp = "";
            f1.richTextBox1.Text = temp;

            while (res < f1.richTextBox1.Text.LastIndexOf(textBox1.Text))
            {
                f1.richTextBox1.Find(textBox1.Text, res, f1.richTextBox1.TextLength, RichTextBoxFinds.None);
                f1.richTextBox1.SelectionBackColor = Color.Blue;
                res = f1.richTextBox1.Text.IndexOf(f1.richTextBox1.Text, res) + 1;
            } 
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
